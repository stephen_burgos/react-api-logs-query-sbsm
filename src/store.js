import sagaPlugin from "kea-saga";

// store.js
import { getStore } from "kea";

export default getStore({
  plugins: [sagaPlugin],
  paths: ["app"]
});
