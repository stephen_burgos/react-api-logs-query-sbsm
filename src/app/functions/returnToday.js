import moment from 'moment';

// Returns current date
function returnToday() {
  return moment().format("YYYY-MM-DD");
}

export default returnToday;