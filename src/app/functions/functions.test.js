import returnToday from "./returnToday";

it("returnToday returns a string", () => {
  expect(typeof returnToday()).toBe("string");
});

it("returnToday returns a string with length 10", () => {
  expect(returnToday().length).toBe(10);
});
