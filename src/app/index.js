import React from "react";
import "antd/dist/antd.css";
import "./index.css";
import InputForm from './components/inputForm';
import { connect } from "kea";
import AppLogic from "./logic";
import Total from "./components/total";
import ProgressDisplay from './components/progressDisplay';

const App = ({ actions, ...props }) => {

  return (
    <div className={"App-header"}>
      <p className={"paragraph"}>{`${props.paragraphReducer}`}</p>
      <div>
        <InputForm />
      </div>
      <Total className={"paragraph"} count={props.fetchReducer} />
      <ProgressDisplay />
    </div>
  );
};

const keaLogic = {
  props: [AppLogic, ["fetchReducer", "paragraphReducer"]]
};

export default connect(keaLogic)(App);
