import React from "react";
import { connect } from "kea";
import AppLogic from "../../logic";
import { Form, DatePicker } from "antd";
import moment from "moment";

const { RangePicker } = DatePicker;

const DateRange = ({ actions, ...props }) => {
  function dateChange(e) {
    // Checks if the user has cleared the date, otherwise convert array elements to strings and send to kea
    try {
      actions.startDateChange(e[0].toDate().toString());
      actions.endDateChange(e[1].toDate().toString());
      actions.setDateComplete(true);
    } catch (error) {
      actions.setDateComplete(false);
      console.warn(error);
    }
  }

  // Configuration for the Date Range field.
  const rangeConfig = {
    initialValue: [moment(props.startReducer), moment(props.endReducer)],
    rules: [
      { type: "array", required: true, message: "Please select Date Range!" }
    ]
  };

  const { getFieldDecorator } = props.decorator;

  return (
    <Form.Item label="Date Range: ">
      {getFieldDecorator("range-picker", rangeConfig)(
        <RangePicker onChange={dateChange} />
      )}
    </Form.Item>
  );
};

const keaLogic = {
  actions: [AppLogic, ["startDateChange", "endDateChange", "setDateComplete"]],
  props: [AppLogic, ["startReducer", "endReducer"]]
};

export default connect(keaLogic)(DateRange);
