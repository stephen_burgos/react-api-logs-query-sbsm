import React from "react";
import { connect } from "kea";
import AppLogic from "../../logic";
import { Form, Button } from "antd";

const SubmitButton = ({ actions, ...props }) => {
  // Confirms that both Account ID and Date Range fields are filled out
  function handleClick() {
    if (props.isAccountComplete && props.isDateComplete) {
      actions.setLoading(true);
    }
  }

  return (
    <Form.Item
      wrapperCol={{
        xs: { span: 24, offset: 9 },
        sm: { span: 16, offset: 9 }
      }}
    >
      <Button
        type="primary"
        htmlType="submit"
        onClick={handleClick}
        loading={props.isLoading}
      >
        Submit
      </Button>
    </Form.Item>
  );
};

const keaLogic = {
  actions: [AppLogic, ["setLoading"]],
  props: [AppLogic, ["isLoading", "isAccountComplete", "isDateComplete"]]
};

export default connect(keaLogic)(SubmitButton);
