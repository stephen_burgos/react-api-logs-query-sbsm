import React from 'react';

// Render a total
const Total = ({count}) => {

  return (
    <div >
      Total: {count}
      <br />
    </div>
  );
};

export default Total;
