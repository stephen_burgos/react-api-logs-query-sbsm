import React from "react";
import { connect } from "kea";
import AppLogic from "../../logic";
import { Form, Input } from "antd";

const ServiceInput = ({ actions, ...props }) => {
  // Update the service field on change
  function servChange(e) {
    actions.serviceChange(e.target.value);
  }

  // Configuration for the Service field.
  const serviceConfig = {
    initialValue: props.serviceReducer,
    rules: [{ type: "string" }]
  };

  const { getFieldDecorator } = props.decorator;

  return (
    <Form.Item label="Service: ">
      {getFieldDecorator("service-input", serviceConfig)(
        <Input onChange={servChange} />
      )}
    </Form.Item>
  );
};

const keaLogic = {
  actions: [AppLogic, ["serviceChange"]],
  props: [AppLogic, ["serviceReducer"]]
};

export default connect(keaLogic)(ServiceInput);
