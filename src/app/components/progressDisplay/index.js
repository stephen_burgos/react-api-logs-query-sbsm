import React from "react";
import { connect } from "kea";
import AppLogic from "../../logic";
import { Progress } from "antd";

const ProgressDisplay = ({ ...props }) => {
  // totalAllowance is hardcoded for now, but is subject to change and should be in the store once it is set
  const totalAllowance = 50000;

  return (
    <div>
      <Progress
        type={"circle"}
        percent={
          Math.round((props.fetchReducer / totalAllowance) * 10000)/100
        }
      />
    </div>
  );
};

const keaLogic = {
  action: [AppLogic, []],
  props: [AppLogic, ["fetchReducer"]]
};

export default connect(keaLogic)(ProgressDisplay);
