import React from "react";
import { connect } from "kea";
import AppLogic from "../../logic";
import { Form, Input } from "antd";

const AccountInput = ({ actions, ...props }) => {
  function accChange(e) {
    actions.accountIdChange(e.target.value);
    // Checks if the user has deleted all text from the Account ID field.
    if (e.target.value === "") {
      console.log("Account Reducer empty");
      actions.setAccountComplete(false);
    } else {
      console.log("Account Reducer holds value");
      actions.setAccountComplete(true);
    }
  }

  // Configuration for the Account ID field.
  const accountConfig = {
    initialValue: props.accountReducer,
    rules: [
      { type: "string", required: true, message: "Please enter Account ID!" }
    ]
  };

  const { getFieldDecorator } = props.decorator;

  return (
    <Form.Item label="Account ID: " style={{ color: "white" }}>
      {getFieldDecorator("account-input", accountConfig)(
        <Input onChange={accChange} />
      )}
    </Form.Item>
  );
};

const keaLogic = {
  actions: [AppLogic, ["accountIdChange", "setAccountComplete"]],
  props: [AppLogic, ["accountReducer"]]
};

export default connect(keaLogic)(AccountInput);
