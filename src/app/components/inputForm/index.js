import React from "react";
import { Form } from "antd";
import AppLogic from "../../logic";
import { connect } from "kea";
import SubmitButton from "../submitButton";
import AccountInput from "../accountInput";
import DateRange from "../dateRange";
import ServiceInput from "../serviceInput";

const InputForm = ({ actions, ...props }) => {
  const handleSubmit = e => {
    e.preventDefault();

    // Validate user input
    props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      } else {
        // Only make an api call if both Account ID and Date Range are filled in
        if (props.isDateComplete && props.isAccountComplete) {
          actions.fetchRequest();
        }
      }

      // Should format date value before submit.
      const rangeValue = fieldsValue["range-picker"];
      const values = {
        ...fieldsValue,
        "range-picker": [
          rangeValue[0].format("YYYY-MM-DD"),
          rangeValue[1].format("YYYY-MM-DD")
        ]
      };
      console.log("inputForm.js received values of form: ", values);
    });
  };

  return (
    <Form onSubmit={handleSubmit}>
      <AccountInput decorator={props.form} />
      <DateRange decorator={props.form} />
      <ServiceInput decorator={props.form} />
      <SubmitButton />
    </Form>
  );
};

// Create the form
const WrappedInputForm = Form.create({ name: "input_form" })(InputForm);

const keaLogic = {
  actions: [AppLogic, ["fetchRequest"]],
  props: [AppLogic, ["isAccountComplete", "isDateComplete"]]
};

export default connect(keaLogic)(WrappedInputForm);
