import { kea } from "kea";
import PropTypes from 'prop-types';
import returnToday from "./functions/returnToday";
import fetchUsageData from "../api/fetchPhrase";
import { put } from "redux-saga/effects";

const keaStuff = kea({
  actions: () => ({
    accountIdChange: accountId => accountId,
    startDateChange: startDate => startDate,
    endDateChange: endDate => endDate,
    serviceChange: service => service,
    fetchRequest: data => data,
    setTotal: something => something,
    setParagraph: something => something,
    setLoading: boolean => boolean,
    setAccountComplete: boolean => boolean,
    setDateComplete: boolean => boolean
  }),

  reducers: ({ actions }) => ({
    /* Used for storing the account from the user */
    accountReducer: [
      "-F3gEki2EeAOjKwDl8iC",
      PropTypes.string,
      {
        [actions.accountIdChange]: (_, payload) => payload
      }
    ],
    /* Used for storing the start date from the user */
    startReducer: [
      "0001-01-01",
      PropTypes.string,
      {
        [actions.startDateChange]: (_, payload) => payload
      }
    ],
    /* Used for storing the end date from the user, initialized with today's date */
    endReducer: [
      returnToday(),
      PropTypes.string,
      {
        [actions.endDateChange]: (_, payload) => payload
      }
    ],
    /* Used for storing the service from the user */
    serviceReducer: [
      "",
      PropTypes.string,
      {
        [actions.serviceChange]: (_, payload) => payload
      }
    ],
    /* Used for storing usage data returned from the api call */
    fetchReducer: [
      "",
      PropTypes.string,
      {
        [actions.setTotal]: (_, payload) => payload
      }
    ],
    /* Used for storing the message portion returned from the api or error messages to the user */
    paragraphReducer: [
      "Enter Request:",
      PropTypes.string,
      {
        [actions.setParagraph]: (_, payload) => payload
      }
    ],
    /* Used for tracking if an api call is in progress */
    isLoading: [
      false,
      PropTypes.bool,
      {
        [actions.setLoading]: (_, boolean) => boolean
      }
    ],
    /* Used for checking if the Account ID field has any input */
    isAccountComplete: [
      true,
      PropTypes.bool,
      {
        [actions.setAccountComplete]: (_, boolean) => boolean
      }
    ],
    /* Used for checking if the Date Range field has any input */
    isDateComplete: [
      true,
      PropTypes.bool,
      {
        [actions.setDateComplete]: (_, boolean) => boolean
      }
    ]
  }),

  // Binds the action to the worker
  takeLatest: ({ actions, workers }) => ({
    [actions.fetchRequest]: workers.axiosPullData
  }),

  workers: {
    /* Worker to make the api call */
    *axiosPullData() {
      const { setTotal, setParagraph, setLoading } = this.actions;
      const id = yield this.get("accountReducer");
      const start = yield this.get("startReducer");
      const end = yield this.get("endReducer");
      const service = yield this.get("serviceReducer");

      try {
        // Fetch data and display it
        yield put(setLoading(true));
        const data = yield fetchUsageData(id, start, end, service);
        console.log("here", data);
        console.log(data.data);
        yield put(setTotal(data.data.data.toString()));
        yield put(setParagraph(data.data.message));
      } catch (error) {
        // If there is no data for a given account, date range or service, report to the user instead of returning empty data
        yield put(setParagraph("No usage for that query."));
        yield put(setTotal("None"));
        console.log(error);
        console.error(
          "____MEM_FAULT_0xe701834791 YOUR COMPUTER IS SHOWING SIGNS OF INTELLIGENCE.  DESTROY IMMEDIATELY"
        );
      }
      yield put(setLoading(false));
    }
  }
});
export default keaStuff;
