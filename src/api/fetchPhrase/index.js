import axios from "axios";

// Fetch data from api-logs\
const fetchUsageData = (id, start, end, service) => {
  let data;
  if (service === "") {
    const url = `https://api.alpha-api.sardius.media/logs/account/${id}?start=${start}&end=${end}`;
    data = axios({ url, method: "get" });
  } else {
    const url = `https://api.alpha-api.sardius.media/logs/account/${id}/service/${service}?start=${start}&end=${end}`;
    data = axios({ url, method: "get" });
  }
  return data;
};

export default fetchUsageData;
